const FN_NEXT = jest.fn(() => { return {}; });

export default jest.fn(() => {
  return {
    next: FN_NEXT,
  };
});
