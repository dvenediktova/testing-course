import app from '../../../app/main.js';

let server;

beforeAll(() => {
  server = app.listen(3000);
});

afterAll(() => {
  server.close();
});

export function serverWrapper() {
  return server;
}
