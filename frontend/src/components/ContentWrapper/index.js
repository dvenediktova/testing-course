import React, { Component } from 'react';

import './index.css';

class ContentWrapper extends Component {
  render() {
    return (
      <article className="content">
        <div class="value">{this.props.value}</div>
        <button onClick={this.props.onNextValue}>Next</button>
      </article>
    );
  }
}

export default ContentWrapper;
